package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList <E> {

	private int size; //size of the list
	private Node<E> header;  //the first node of the list
	private Comparator <E> comp ; 
	public CircularSortedDoublyLinkedList () {
		this.size = 0;
		this.header = new Node<E>(null);
		header.setNext(header);
		header.setPrevious(header);		
	}
	
	public CircularSortedDoublyLinkedList (Comparator c) {
		this.comp = c;
		this.size = 0;
		this.header = new Node<E>(null);
		header.setNext(header);
		header.setPrevious(header);		
	}
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node (E element) {
			this.element=element;
			this.next = next;
			this.previous = previous;
		}

		public Node(E element, Node<E> next, Node<E> previous) {
			this.element = element;
			this.next = next;
			this.previous= previous;
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrevious(){
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}

	}
	@Override
	public Iterator<E> iterator() {

		return null;
	}

	@Override
	public boolean add(E obj) {
		Node <E> next = new Node <E> (obj);
		if(this.isEmpty()) {
			header.setNext(next);
			header.setPrevious(next);
			next.setPrevious(header);
			next.setNext(header);
			size++;
			return true;
		}
		Node <E> temp = header.getNext();	

		while(temp != header && comp.compare(temp.getElement(), next.getElement()) < 0) {
			temp = temp.getNext();
		}
		next.setNext(temp);
		next.setPrevious(temp.getPrevious());
		temp.getPrevious().setNext(next);
		temp.setPrevious(next);
		size++;
		return true;

	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		if(this.isEmpty()) {
			return false;
		}
		Node <E> temp = header.getNext();

		while(temp != header &&	!temp.getElement().equals(obj)) {
			temp = temp.getNext();
		}
		if( temp == header) return false;
		else {
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		size--;
		
		return true;
		}
	}

	@Override
	public boolean remove(int index) {
		if((index < 0) || (index >= this.size)) {
			throw new IndexOutOfBoundsException("Remove: " + index);

		}
		Node<E> temp = header.getNext();
		for(int i = 0; i  < index;i++) {
			
			temp = temp.getNext();
		}
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		if(this.isEmpty()) return null;
		return this.header.next.getElement();
	}

	@Override
	public E last() {
		if(this.isEmpty()) return null;
		return this.header.previous.getElement();
	}

	@Override
	public E get(int index) {
		if( index < 0 || index >= size)
			throw new IndexOutOfBoundsException ("get: " + index);
		Node<E> temp = header.getNext();
		for(int i = 0; i< index; i ++) {
			temp = temp.getNext();
		}
		return temp.getElement();

	}

	@Override
	public void clear() {
		while (!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		if(this.isEmpty()) return false;
		
		Node<E> temp = header.getNext();

		while(!(temp==header) && !temp.getElement().equals(e)) {

			temp = temp.getNext();

		}
		if(temp == header) return false;
		else {
			return true;
		}

	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public int firstIndex(E e) {
		int index = 0;

		Node<E> temp = header.getNext();
		while(temp !=header && !temp.getElement().equals(e)) {
			temp = temp.getNext();
			index++;
		}
		if(temp != header && index >=0)
			return index;
		else {
			return -1;
		}
	}

	@Override
	public int lastIndex(E e) {

		int index  = size -1;
		Node<E> temp = header.getPrevious();
		while(temp != header && !temp.getElement().equals(e) ) {
			temp = temp.getPrevious();
			index--;
		}
		if(index <= size -1  && temp != header )
			return index;
		else {
			return -1;
		}
	}
	
}
