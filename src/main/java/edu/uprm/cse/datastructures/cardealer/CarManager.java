package edu.uprm.cse.datastructures.cardealer;

import java.util.Optional;
import java.util.function.Predicate;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {
	private final CircularSortedDoublyLinkedList <Car> carList =  CarList.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car [] carArray = new Car[carList.size()];
		for(int i = 0; i<carArray.length;i++) {
			carArray[i] = carList.get(i);
		}
		return carArray;
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar (Car car) {
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId()== car.getCarId()  || carList.get(i).getCarBrand()  == car.getCarBrand()|| carList.get(i).getCarModel()==car.getCarModel()) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
		}
		carList.add(car);
		return Response.status(201).build();
	}
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id)  {
		boolean found = false;
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == id) {
				carList.remove(i);
				found = true;
				return Response.status(200).build();
				
			}
		}if(!found) {
			throw new NotFoundException();
		}
		return Response.status(404).build();
	}
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == car.getCarId()) {
				carList.remove(carList.get(i));
				carList.add(car);
			}else {
				return Response.status(404).build();
			}
		}
		return Response.status(200).build();
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id)  {

		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId()==id) {
				return carList.get(i);
			}
		}
		throw new NotFoundException();

	}
}
