package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<>(new CarComparator());

	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return carList;
		
	}
	public static void resetCars() {
			carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
			carList.clear();
	}

}
